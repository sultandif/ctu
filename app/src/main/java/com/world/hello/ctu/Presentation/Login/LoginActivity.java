package com.world.hello.ctu.Presentation.Login;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.world.hello.ctu.Database.DBHelper;
import com.world.hello.ctu.Network.APIConnection;
import com.world.hello.ctu.R;

public class LoginActivity extends ActionBarActivity {

    private EditText mLoginUsernameEditText;
    private EditText mLoginPasswordEditText;
    private Button mLoginUsernameButton;

    public static final String USERNAME_EXTRA = "username_extra";
    public static final String PASSWORD_EXTRA = "password_extra";

    private DBHelper mDB;
    private APIConnection mAPI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mLoginUsernameEditText = (EditText) findViewById(R.id.loginUsernameEditText);
        mLoginPasswordEditText = (EditText) findViewById(R.id.loginPasswordEditText);
        mLoginUsernameButton = (Button) findViewById(R.id.loginUsernameButton);

        //mDB = DBHelper.getInstance(this);
        mAPI = APIConnection.getInstance();

        mLoginUsernameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = mLoginUsernameEditText.getText().toString();
                String password = mLoginPasswordEditText.getText().toString();

                boolean result = mAPI.setUsernamePassword(username, password);
                if(result) {
                    // correct name and password
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra(USERNAME_EXTRA, username);
                    returnIntent.putExtra(PASSWORD_EXTRA, password);
                    setResult(RESULT_OK, returnIntent);
                    finish();

                } else {
                    // incorrect name and password
                    return;
                }


                //mDB.addUser(username, password);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
