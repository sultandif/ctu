package com.world.hello.ctu.Presentation.Timetable;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.world.hello.ctu.Model.TimetableSlot;
import com.world.hello.ctu.R;

public class AddSlotActivity extends ActionBarActivity {

    private EditText mSubjectNameEditText;
    private Spinner mSubjectTypeSpinner;
    private Button mAddSubjectButton;

    private Intent mTimetableIntent;

    public static final String SUBJECT_NAME_EXTRA = "subject_name_extra";
    public static final String SUBJECT_TYPE_EXTRA = "subject_type_extra";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_slot);

        mTimetableIntent = getIntent();

        mSubjectNameEditText = (EditText) findViewById(R.id.subjectNameEditText);
        mSubjectTypeSpinner = (Spinner) findViewById(R.id.subjectTypeSpinner);
        mAddSubjectButton = (Button) findViewById(R.id.addSubjectButton);
        mAddSubjectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();

                Bundle extras = mTimetableIntent.getExtras();
                int weekDay = extras.getInt(WeekTimetableFragment.WEEK_DAY_EXTRA);
                int dayTime = extras.getInt(WeekTimetableFragment.DAY_TIME_EXTRA);
                String parity = extras.getString(WeekTimetableFragment.PARITY_EXTRA);

                String subjectName = mSubjectNameEditText.getText().toString();
                String subjectType = String.valueOf(mSubjectTypeSpinner.getSelectedItem());

                returnIntent.putExtra(SUBJECT_NAME_EXTRA, subjectName);
                returnIntent.putExtra(SUBJECT_TYPE_EXTRA, subjectType);
                returnIntent.putExtra(WeekTimetableFragment.WEEK_DAY_EXTRA, weekDay);
                returnIntent.putExtra(WeekTimetableFragment.DAY_TIME_EXTRA, dayTime);
                returnIntent.putExtra(WeekTimetableFragment.PARITY_EXTRA, parity);

                setResult(RESULT_OK, returnIntent);
                finish();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_slot, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
