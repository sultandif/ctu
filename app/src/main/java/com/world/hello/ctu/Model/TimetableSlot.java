package com.world.hello.ctu.Model;

/**
 * Created by Sultan on 08.05.2015.
 */
public class TimetableSlot {
    public static final int MONDAY = 0;
    public static final int TUESDAY = 1;
    public static final int WEDNESDAY = 2;
    public static final int THURSDAY = 3;
    public static final int FRIDAY = 4;
    public static final int SATURDAY = 5;
    public static final int SUNDAY = 6;

    public static final String EVEN = "even";
    public static final String ODD = "odd";

    public static final String LECTURE = "Lecture";
    public static final String PRACTICE = "Practice";
    public static final String SEMINAR = "Seminar";

    private final String mSubject;
    private final String mType;
    private final int mWeekDay;
    private final int mDayTime;
    private final String mParity;

    public TimetableSlot(String subject, String type, int weekDay, int dayTime, String parity) {
        this.mSubject = subject;
        this.mType = type;
        this.mDayTime = dayTime;
        this.mWeekDay = weekDay;
        this.mParity = parity;
    }

    public String getSubject() {
        return mSubject;
    }

    public String getType() { return mType; }

    public int getTime() {
        return mDayTime;
    }

    public int getDay() { return mWeekDay; }

    public String getParity() {
        return mParity;
    }
}
