package com.world.hello.ctu.Presentation.Timetable;

import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.world.hello.ctu.Model.Timetable;
import com.world.hello.ctu.Model.TimetableSlot;
import com.world.hello.ctu.R;

/**
 * Created by Sultan on 08.05.2015.
 */
public class TimetableGridAdapter extends BaseAdapter {

    private final Context mContext;
    private final Timetable mTimetable;

    public TimetableGridAdapter(Context context, Timetable timetable) {
        this.mContext = context;
        this.mTimetable = timetable;
    }

    @Override
    public int getCount() {
        return 8 * 14;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = null;

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(convertView == null) {
            itemView = inflater.inflate(R.layout.item_timetable, null);

        } else {
            itemView = (View) convertView;
        }

        TextView subjectName = (TextView) itemView.findViewById(R.id.itemTimetableSubjectName);
        int dayOfWeek = position % 8;
        int timeOfDay = position / 8 + 1;
        TimetableSlot subject = null;
        if(dayOfWeek != 0) subject = mTimetable.getSubject(dayOfWeek - 1, timeOfDay);
        if(subject == null) {
            // there is no subject at this time
            subjectName.setText("");
            itemView.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.empty_item_background));

            if(dayOfWeek == 0) {
                subjectName.setGravity(Gravity.CENTER);
                int minutes = 7 * 60 + 30;
                minutes += (timeOfDay - 1) * 45 + (timeOfDay / 2) * 15;
                int hours = minutes / 60;
                minutes = minutes % 60;
                subjectName.setText(String.format("%02d", hours)  + "\n:\n" + String.format("%02d", minutes));
                //subjectName.setText(Integer.toString(timeOfDay /*+ 1*/));
                itemView.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.time_item_background));
            }
        } else {
            // there is subject at this time
            subjectName.setText(subject.getSubject());
            if(subject.getType().equals(TimetableSlot.LECTURE)) {
                itemView.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.lecture_item_background));
                subjectName.setGravity(Gravity.CENTER);
            }
            if(subject.getType().equals(TimetableSlot.PRACTICE)) {
                itemView.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.practice_item_background));
                subjectName.setGravity(Gravity.CENTER);
            }
            if(subject.getType().equals(TimetableSlot.SEMINAR)) {
                itemView.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.seminar_item_background));
                subjectName.setGravity(Gravity.CENTER);
            }
        }



        return itemView;
    }
}
