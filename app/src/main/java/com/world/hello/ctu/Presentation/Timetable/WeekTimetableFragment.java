package com.world.hello.ctu.Presentation.Timetable;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.world.hello.ctu.Model.Timetable;
import com.world.hello.ctu.Model.TimetableSlot;
import com.world.hello.ctu.R;

public class WeekTimetableFragment extends Fragment {

    private static final String PARITY = "parity";
    public static final String EVEN = "Even Week";
    public static final String ODD = "Odd Week";
    public static final int COUNT = 2;

    public static final String WEEK_DAY_EXTRA = "week_day_extra";
    public static final String DAY_TIME_EXTRA = "day_time_extra";
    public static final String PARITY_EXTRA = "parity_extra";

    private String mParity;

    private OnFragmentInteractionListener mListener;

    private View mView;
    private GridView mGridView;
    private TextView mParityTextView;

    public static WeekTimetableFragment newInstance(String mParity) {
        WeekTimetableFragment fragment = new WeekTimetableFragment();
        Bundle args = new Bundle();
        args.putString(PARITY, mParity);
        fragment.setArguments(args);
        return fragment;
    }

    public WeekTimetableFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParity = getArguments().getString(PARITY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_week_timetable, container, false);
        mView = view;
        return mView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mGridView = (GridView) mView.findViewById(R.id.timetableGridView);
        mParityTextView = (TextView) mView.findViewById(R.id.parityTextView);
        String parity = getArguments().getString(PARITY);
        mParityTextView.setText(parity);
        TimetableGridAdapter adapter = mListener.onFragmentInteraction(this, parity);
        mGridView.setAdapter(adapter);

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            }
        });

        mGridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                final int dayOfWeek = position % 8 - 1;
                final int timeOfDay = position / 8 + 1;
                Timetable timetable = null;
                String parity = null;
                if(mListener.getParity() == 0) {
                    timetable = mListener.getTimetable(TimetableSlot.EVEN);
                    parity = TimetableSlot.EVEN;
                } else {
                    timetable = mListener.getTimetable(TimetableSlot.ODD);
                    parity = TimetableSlot.ODD;
                }

                if(timetable.getSubject(dayOfWeek, timeOfDay) != null) {
                    // there is a slot in this position
                    // delete slot
                    /*
                    Activity activity = (Activity) mListener;
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity.getApplicationContext());
                    dialogBuilder.setTitle("Timetable Slot");
                    dialogBuilder.
                            setMessage("Delete this slot?")
                            .setCancelable(true)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // delete slot here
                                    mListener.deleteTimetableSlotFromDatabase(dayOfWeek, timeOfDay);
                                    String parity = null;
                                    if(mListener.getParity() == 0) {
                                        parity = TimetableSlot.EVEN;
                                    } else {
                                        parity = TimetableSlot.ODD;
                                    }
                                    mListener.getTimetable(parity).deleteSubject(dayOfWeek, timeOfDay);
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog dialog = dialogBuilder.create();
                    dialog.show();
                    */

                } else {
                    // there is no slot in this position
                    // adding new slot
                    Intent intent = new Intent((Activity) mListener, AddSlotActivity.class);
                    intent.putExtra(WEEK_DAY_EXTRA, dayOfWeek);
                    intent.putExtra(DAY_TIME_EXTRA, timeOfDay);
                    intent.putExtra(PARITY_EXTRA, parity);
                    startActivityForResult(intent, 1);
                }
                return false;
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bundle extras = data.getExtras();
        String subjectName = extras.getString(AddSlotActivity.SUBJECT_NAME_EXTRA);
        String subjectType = extras.getString(AddSlotActivity.SUBJECT_TYPE_EXTRA);
        int weekDay = extras.getInt(WEEK_DAY_EXTRA);
        int dayTime = extras.getInt(DAY_TIME_EXTRA);
        String parity = extras.getString(PARITY_EXTRA);

        TimetableSlot slot = new TimetableSlot(subjectName, subjectType, weekDay, dayTime, parity);
        mListener.addTimetableSlotToDatabase(slot);

        Timetable timetable = mListener.getTimetable(parity);
        timetable.addSubject(slot);


    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        public TimetableGridAdapter onFragmentInteraction(WeekTimetableFragment fragment, String parity);
        public Timetable getTimetable(String parity);
        public int getParity();
        public void addTimetableSlotToDatabase(TimetableSlot slot);
        public void deleteTimetableSlotFromDatabase(int weekDay, int dayTime);
    }

}
