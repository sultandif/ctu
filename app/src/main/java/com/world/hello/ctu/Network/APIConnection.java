package com.world.hello.ctu.Network;

import com.world.hello.ctu.Model.TimetableSlot;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sultan on 09.05.2015.
 */
public class APIConnection {

    private final HttpClient mClient;
    private InputStream mInputStream;
    private String mUsername = null;
    private String mPassword = null;

    private static APIConnection mInstance = null;

    private APIConnection() {
        this.mClient = new DefaultHttpClient();
    }

    public static APIConnection getInstance() {
        if(mInstance == null){
            mInstance = new APIConnection();
        }
        return mInstance;
    }

    private boolean checkPassword(String username, String password) {

        return true;
    }

    public boolean setUsernamePassword(String username, String password) {
        // check username and password
        boolean result = checkPassword(username, password);
        if(result) {
            this.mUsername = username;
            this.mPassword = password;
            return result;
        } else {
            return false;
        }
    }

    public List<String> getParallels() {
        List<String> parallels = new ArrayList<>();

        String url = "https://kosapi.fit.cvut.cz/api/3/students/" + mUsername + "/parallels/";
        try {
            HttpResponse response = mClient.execute(new HttpGet(url));
            mInputStream = response.getEntity().getContent();
            if(mInputStream != null) {

            }
        } catch(IOException e) {
            e.printStackTrace();
        }

        return parallels;
    }

    public List<TimetableSlot> getTimetableSubjects() {
        List<TimetableSlot> subjects = new ArrayList<>();


        return subjects;
    }

}
