package com.world.hello.ctu.Presentation.Main;

import android.app.Activity;
import android.content.Context;
import android.opengl.GLSurfaceView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.world.hello.ctu.R;

/**
 * Created by Sultan on 09.05.2015.
 */
public class DrawerItemAdapter extends ArrayAdapter<String> {

    private Context mContext;
    private int mLayoutResourceId;
    String mData[] = null;

    public DrawerItemAdapter(Context context, int layoutResourceId, String[] data) {
        super(context, layoutResourceId, data);
        this.mContext = context;
        this.mLayoutResourceId = layoutResourceId;
        this.mData = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View listItem = convertView;
        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        listItem = inflater.inflate(mLayoutResourceId, parent, false);
        TextView textView = (TextView) listItem.findViewById(R.id.drawerItemTextView);
        String item = mData[position];
        textView.setText(item);
        return listItem;
    }
}
