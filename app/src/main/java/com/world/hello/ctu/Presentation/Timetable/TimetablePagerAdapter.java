package com.world.hello.ctu.Presentation.Timetable;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.SparseArray;

/**
 * Created by Sultan on 08.05.2015.
 */
public class TimetablePagerAdapter extends FragmentPagerAdapter {

    private final FragmentManager mFragmentManager;
    private SparseArray<Fragment> registeredFragments;

    public TimetablePagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
        this.mFragmentManager = fragmentManager;
        registeredFragments = new SparseArray<>();
    }

    @Override
    public Fragment getItem(int id) {
        Fragment fragment;
        if(id == 0) fragment = WeekTimetableFragment.newInstance(WeekTimetableFragment.EVEN);
        else fragment = WeekTimetableFragment.newInstance(WeekTimetableFragment.ODD);
        registeredFragments.put(id, fragment);
        return fragment;
    }

    @Override
    public int getCount() {
        return WeekTimetableFragment.COUNT;
    }

    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }

}
