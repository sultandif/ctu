package com.world.hello.ctu.Model;

import android.util.SparseArray;

/**
 * Created by Sultan on 08.05.2015.
 */
public class Timetable {

    private SparseArray<TimetableSlot> mSubjects;
    private int mPointer;

    public Timetable() {
        mSubjects = new SparseArray<>();
        for(int i = 0; i < 12; i ++) {
            for(int j = 0; j < 7; j ++) {
                mSubjects.put(i * 7 + j, null);
            }
        }
        mPointer = 0;
    }

    public void addSubject(TimetableSlot subject) {
        //mSubjects.put(timeOfDay * 7 + dayOfWeek, subject);
        mSubjects.put(subject.getTime() * 7 + subject.getDay(), subject);
    }

    public TimetableSlot getSubject(int dayOfWeek, int timeOfDay) {
        return mSubjects.get(timeOfDay * 7 + dayOfWeek);
    }

    public void deleteSubject(int weekDay, int dayTime) {
        mSubjects.delete(dayTime * 7 + weekDay);
    }

    /*
    public TimetableSubject getNextSubject() {
        return mSubjects.get(mPointer ++);
    }
    */

}
