package com.world.hello.ctu.Presentation.Timetable;

import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.world.hello.ctu.Database.DBHelper;
import com.world.hello.ctu.Model.Timetable;
import com.world.hello.ctu.Model.TimetableSlot;
import com.world.hello.ctu.R;

import java.util.List;


public class TimetableActivity extends ActionBarActivity implements WeekTimetableFragment.OnFragmentInteractionListener {

    private ViewPager mViewPager;
    private TimetablePagerAdapter mPagerAdapter;
    private DBHelper mDB;
    private Timetable mTimetableEven;
    private Timetable mTimetableOdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timetable);

        // deleting database
        //deleteDatabase(DBHelper.DATABASE_NAME);

        mViewPager = (ViewPager) findViewById(R.id.weekTimetablePager);
        mPagerAdapter = new TimetablePagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mPagerAdapter);

        //mDB = new DBHelper(this);
        mDB = DBHelper.getInstance(this);
        mTimetableEven = new Timetable();
        mTimetableOdd = new Timetable();

        // just for testing
        //mDB.deleteAllTimetableSlots();

        TimetableSlot mathLecture1 = new TimetableSlot("Math", TimetableSlot.LECTURE, TimetableSlot.THURSDAY, 4, TimetableSlot.EVEN);
        TimetableSlot mathPractice1 = new TimetableSlot("Math", TimetableSlot.PRACTICE, TimetableSlot.SUNDAY, 1, TimetableSlot.EVEN);
        TimetableSlot mathSeminar1 = new TimetableSlot("Math", TimetableSlot.SEMINAR, TimetableSlot.TUESDAY, 3, TimetableSlot.ODD);

        mDB.addTimetableSlot(mathLecture1);
        mDB.addTimetableSlot(mathPractice1);
        mDB.addTimetableSlot(mathSeminar1);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public TimetableGridAdapter onFragmentInteraction(WeekTimetableFragment fragment, String parity) {
        TimetableGridAdapter gridAdapter = null;
        List<TimetableSlot> slots = mDB.getAllTimetableSlots();

        if(parity.equals(WeekTimetableFragment.EVEN)) {
            for(TimetableSlot slot : slots) {
                if(slot.getParity().equals(TimetableSlot.EVEN)) {
                    mTimetableEven.addSubject(slot);
                }
            }
            gridAdapter = new TimetableGridAdapter(this, mTimetableEven);
        } else {
            for(TimetableSlot slot : slots) {
                if(slot.getParity().equals(TimetableSlot.ODD)) {
                    mTimetableOdd.addSubject(slot);
                }
            }
            gridAdapter = new TimetableGridAdapter(this, mTimetableOdd);
        }

        return gridAdapter;
    }

    @Override
    public Timetable getTimetable(String parity) {
        if(parity.equals(TimetableSlot.EVEN)) return mTimetableEven;
        else return mTimetableOdd;
    }

    @Override
    public int getParity() {
        return mViewPager.getCurrentItem();
    }

    @Override
    public void addTimetableSlotToDatabase(TimetableSlot slot) {
        mDB.addTimetableSlot(slot);
    }

    @Override
    public void deleteTimetableSlotFromDatabase(int weekDay, int dayTime) {
        mDB.deleteTimetableSlot(weekDay, dayTime);
    }
}
