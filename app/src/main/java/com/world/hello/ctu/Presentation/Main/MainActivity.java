package com.world.hello.ctu.Presentation.Main;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.world.hello.ctu.Database.DBHelper;
import com.world.hello.ctu.Network.APIConnection;
import com.world.hello.ctu.Presentation.Login.LoginActivity;
import com.world.hello.ctu.Presentation.Timetable.TimetableActivity;
import com.world.hello.ctu.R;

public class MainActivity extends ActionBarActivity {

    //private String[] mNavigationDrawerItemTitles;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    private String[] mDrawerItems = {
            "CTU", "Timetable", "Help", "Sign Out"
    };

    private DBHelper mDB;
    private APIConnection mAPI;
    private String mUsername;
    private int mCurrentItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // just for testing
        //deleteDatabase(DBHelper.DATABASE_NAME);

        //mNavigationDrawerItemTitles = getResources().getStringArray(R.array.navigation_drawer_items_array);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mDrawerList = (ListView) findViewById(R.id.leftDrawer);

        mDB = DBHelper.getInstance(this);
        mAPI = APIConnection.getInstance();

        /*
        String[] drawerItems = new String[3];
        drawerItems[0] = "CTU";
        drawerItems[1] = "Timetable";
        drawerItems[2] = "Help";
        */



        DrawerItemAdapter adapter = new DrawerItemAdapter(this, R.layout.navigation_drawer_item, mDrawerItems);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                //toolbar,
                R.string.drawer_open,
                R.string.drawer_close
        ) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getSupportActionBar().setTitle(mDrawerItems[mCurrentItem]);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle(mUsername);
            }
        };

        mDrawerToggle.syncState();
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        // default fragment (Main Page)
        setMainFragment();

        String username = mDB.getUser();
        if(username != null) {
            mUsername = username;
        } else {
            startLoginActivity();
        }

    }

    private void setMainFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.contentFrame, new MainFragment()).commit();
        mDrawerList.setItemChecked(0, true);
        mDrawerList.setSelection(0);
        getSupportActionBar().setTitle(mDrawerItems[0]);
        mCurrentItem = 0;
    }

    private void startLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 1) {
            if(resultCode == RESULT_OK) {
                String username = data.getStringExtra(LoginActivity.USERNAME_EXTRA);
                String password = data.getStringExtra(LoginActivity.PASSWORD_EXTRA);
                mUsername = username;
                mDB.setUser(username, password);
                setMainFragment();
            }
            if(resultCode == RESULT_CANCELED) {

            }
        }
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    private void selectItem(int position) {
        Fragment fragment = null;
        mCurrentItem = position;
        switch(position) {
            case 0:
                // Main menu
                fragment = new MainFragment();
                break;
            case 1:
                // Start timetable activity
                Intent intent = new Intent(this, TimetableActivity.class);
                startActivity(intent);
                break;
            case 2:
                // Help activity

                break;
            case 3:
                // Sign out
                mDB.deleteUser();
                mDB.deleteAllTimetableSlots();
                startLoginActivity();
                break;
        }

        if(fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.contentFrame, fragment).commit();
            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);
            getSupportActionBar().setTitle(mDrawerItems[position]);
            mDrawerLayout.closeDrawer(mDrawerList);
        } else {

        }
    }

    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if(mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
