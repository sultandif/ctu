package com.world.hello.ctu.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.world.hello.ctu.Model.TimetableSlot;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sultan on 09.05.2015.
 */
public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "CTU_Database";
    public static final int DATABASE_VERSION = 1;
    public static final String TIMETABLE_SLOTS_TABLE_NAME = "timetable_slots";
    public static final String TIMETABLE_SLOTS_COLUMN_ID = "id";
    public static final String TIMETABLE_SLOTS_COLUMN_SUBJECT = "subject";
    public static final String TIMETABLE_SLOTS_COLUMN_TYPE = "type";
    public static final String TIMETABLE_SLOTS_COLUMN_WEEKDAY = "weekday";
    public static final String TIMETABLE_SLOTS_COLUMN_DAYTIME = "daytime";
    public static final String TIMETABLE_SLOTS_COLUMN_PARITY = "parity";

    private static DBHelper mInstance = null;

    private Context mContext;

    private DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.mContext = context;

        // just for testing
        //deleteAllTimetableSlots();
    }

    public static DBHelper getInstance(Context context) {
        if(mInstance == null) {
            mInstance = new DBHelper(context);
        }
        return mInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // creating user table
        String query = "CREATE TABLE user " +
                "(id INTEGER PRIMARY KEY, username TEXT, password TEXT)";
        db.execSQL(query);

        // creating timetable_slots table
        query = "CREATE TABLE timetable_slots " +
                "(id INTEGER PRIMARY KEY, subject TEXT, type TEXT, weekday INTEGER, daytime INTEGER, parity TEXT)";
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS timetable_slots");
        onCreate(db);
    }

    public void setUser(String username, String password) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put("username", username);
        contentValues.put("password", password);

        db.insert("user", null, contentValues);
    }

    public String getUser() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM user";
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst(); // !!!
        if(cursor.getCount() > 0) {
            // user exists
            String username = cursor.getString(cursor.getColumnIndex("username"));
            return username;
        } else {
            // user doesn't exist
            return null;
        }
    }

    public void deleteUser() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM user";
        db.execSQL(query);
    }

    public boolean addTimetableSlot(TimetableSlot slot) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put("subject", slot.getSubject());
        contentValues.put("type", slot.getType());
        contentValues.put("weekday", slot.getDay());
        contentValues.put("daytime", slot.getTime());
        contentValues.put("parity", slot.getParity());

        db.insert("timetable_slots", null, contentValues);
        return true;
    }

    public List<TimetableSlot> getAllTimetableSlots() {
        List<TimetableSlot> slots = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM timetable_slots";
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while(cursor.isAfterLast() == false) {
            String subject = cursor.getString(cursor.getColumnIndex("subject"));
            String type = cursor.getString(cursor.getColumnIndex("type"));
            int weekDay = cursor.getInt(cursor.getColumnIndex("weekday"));
            int dayTime = cursor.getInt(cursor.getColumnIndex("daytime"));
            String parity = cursor.getString(cursor.getColumnIndex("parity"));

            TimetableSlot slot = new TimetableSlot(subject, type, weekDay, dayTime, parity);
            slots.add(slot);

            cursor.moveToNext();
        }

        return slots;
    }

    public void deleteAllTimetableSlots() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM timetable_slots";
        db.execSQL(query);
    }

    public void deleteTimetableSlot(int weekDay, int dayTime) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM timetable_slots WHERE weekday = " + weekDay + " AND daytime = " + dayTime;
        db.execSQL(query);
    }

}
